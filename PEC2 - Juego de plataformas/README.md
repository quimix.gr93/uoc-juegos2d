# PEC2 -  Platform Game

Proyecto que corresponde al desarrollo de la PEC2 - Un juego de plataformas para la asignatura Programación de juegos Unity 2D de la UOC del semestre febrero 2020

---

## Índice

1. [Introducción](## Introducción)
2. [Planteamiento del juego](##Planteamiento-del-juego)
3. [Escenas del juego](##Escenas-del-juego)
4. [Detalles del desarrollo](##Detalles-del-desarrollo)
   1. [Personaje: movimiento y salto](###4.1. Personaje: movimiento y salto)
   2. [Personaje: cabeza y pies](###4.2. Personaje: cabeza y pies)
   3. [Enemigos](###4.3. Enemigos)
   4. [Puntos de aparición de los enemigos](###4.4. Puntos de aparición de los enemigos)
   5. [Bloques interactivos](###4.5. Bloques interactivos)
   6. [SuperMode](###4.6. SuperMode)
5. [Cómo jugar](##Cómo jugar)

---

1. ## Introducción

El proyecto que se ha desarrollado consiste en la réplica del nivel 1 - 1 del juego Super Mario Bros de Nintendo. Se ha hecho un juego plataformas en 2D en el que el jugador debe llegar al final del nivel para superarlo. El jugador puede conseguir puntos mediante la activación de ciertos bloques del nivel o eliminando los enemigos que se encuentra.

Además, el jugador deberá ir con cuidado de no caer por los agujeros del mapa ni de colisionar con los enemigos puesto que si se diese el caso, perdería.

2. ## Planteamiento del juego

A partir de la información que se proporciona en el enunciado de la [PEC](https://gitlab.com/uoc-unity-2d-2020/pec2), se han tomado las siguientes decisiones al respecto del juego:

* Se desarrollaran unas mecánicas de movimiento, desplazamiento horizontal y salto, gracias a la aplicación de físicas del motor Unity. Para el desarrollo del salto, al igual que en los títulos de Mario, se ha hecho que a mayor tiempo de pulsación de la tecla mayor sea el salto.
* Juego de scroll horizontal con una cámara que sigue al personaje. Igual que en Mario, se ha hecho que el jugador no pueda retroceder más allá del punto más a la izquierda mostrado por la cámara y tiene un pequeño ajuste en el eje vertical si el jugador supera una altura concreta.
* Al igual que en los juegos 2D de Mario, si los enemigos colisionan con un objeto del entorno, cambian la dirección de movimiento.
* Se han implementado tres bloques interactivos para con el jugador. Dichos tipos de bloques son:
  * de piedra: son destructibles para el jugador si impacta con la cabeza del personaje habiendo consumido una estrella.
  * de exclamación: proporcionan al jugador un ítem (estrella) para proporcionarle el "SuperMode". En caso de que el jugador ya haya consumido una estrella y mantenga el estado "SuperMode", éste bloque suelta una moneda que otorga mayor puntuación que los bloques moneda.
  * de moneda: si el jugador colisiona con la cabeza con uno de éstos bloques, se activa un efecto y suman puntos al jugador.
* El jugador sólo tiene un punto de vida el cuál puede ser quitado por los enemigos si colisionan con el personaje. Puede conseguirse un punto de vida extra entrando en "SuperMode". Aún si el jugador tiene dicho punto de vida opcional, si cae por uno de los agujeros del mapa pierde.
* Siguiendo con el ejemplo de Mario, si el jugador consigue el "SuperMode" hay apreciación visual y auditiva de dicho  cambio. Se ha optado por añadir partículas que siguen al jugador en vez de incrementar su tamaño y se ha modificado la característica "pitch" del clip de música para conseguir el efecto indicativo.

Se han creado dos tipos de objetos coleccionables por parte del jugador. Ambos son soltados por los bloques exclamación y son los siguientes:

* Estrella: objeto que activa el "SuperMode". Sus efectos son otorgar un punto extra de vida al jugador.
* Moneda: objeto que otorga la mayor cantidad de puntos en el juego.

El sistema de puntuaciones implementado para éste nivel ha sido el siguiente:

* 100 puntos por la eliminación de un enemigo.
* 150 puntos por la activación de un bloque de moneda.
* 250 puntos por la adquisición de una moneda procedente de un bloque de exclamación.

3. ## Escenas del juego

Se ha seguido con el ejemplo de la plantilla de la PEC2 para realizar un juego completo a partir de una única escena. Ésta escena cuenta con los diferentes menús del juego:

* Menú inicio: se puede apreciar el título del juego en una pequeña interfaz y dos botones. Uno para empezar a jugar y otro para salir del juego.
* Menú GameOver/Victoria: se puede ver un título, puede variar entre "GameOver" y "You Win!" dependiendo de si el jugador ha conseguido completar el nivel con éxito, los puntos conseguidos en el intento actual, el récord conseguido por el jugador y dos botones. Uno de los botones devuelve el juego a su estado inicial y el otro permite salir del juego.

A parte de los menús, hay dos UI extra en el juego:

* La interfície que muestra la cuenta atrás.
* La interfície que se muestra durante el juego. Es una interfície sencilla que muestra los puntos del jugador.

4. ## Detalles del desarrollo

Para el desarrollo del juego se ha usado:

* El paquete TextMeshPro para crear textos y botones con mayor definición y más personalizables.
* El paquete "Platformer Pack Redux" de assets de Kenny's del que se han sacado personajes y sprites para objetos y dibujo del mapa.
* Se ha partido del tutorial de Blackthornprod en youtube para el desarrollo de la [mecánica de salto](https://www.youtube.com/watch?v=j111eKN8sJw) (sistema a partir del que se puede mantener el botón para saltar más o menos).
* Se ha partido del tutorial de Code Monkey en youtube para el desarrollo de un fondo de pantalla que realiza scroll infinito gracias al [efecto Parallax](https://www.youtube.com/watch?v=wBol2xzxCOU)
* Uso de los Tilemaps y Tile Palettes para dibujar el escenario del juego de forma sencilla (se puede encontrar en el Package Manager de Unity).

### 4.1. Personaje: movimiento y salto

Para el desarrollo de esos aspectos del personaje, he aprovechado el tipo de componente "Rigidbody2D" con el que se permite afectar con físicas al objeto al que esté ligado.

De esta manera, con el "InputAxis" horizontal y un valor de velocidad, se ha conseguido el desplazamiento horizontal del personaje mediante el cambio del vector "velocity" del "Rigidbody".

Para el salto, comprobamos si el personaje se encuentra en el suelo para permitir hacerlo y se ha añadido a la implementación un contador del tiempo que ha mantenido el usuario el botón de salto. Dicha solución consta de lo siguiente:

* Una comprobación en el método "Update" que mira, gracias al método "OverlapCircle" de la librería "Physics2D", si los pies del jugador están en colisión con algún objeto considerado como suelo.
* Si se cumple con la condición anterior y se presiona el botón "Space", se aplica una fuerza en el eje Y para impulsar al personaje a modo de salto. En este punto se reinicia el contador de tiempo de salto y se activa un boolean que informa de que el jugador está saltando. Además se reproduce un sonido y se cambia el estado de una de las variables del controlador de animaciones para que haya transición de un estado a otro.
* Se comprueba que si el jugador ha saltado y sigue manteniendo el botón de salto. En dicho caso, se sigue aplicando la fuerza y se resta tiempo al contador.
* En cuando se deja de apretar el botón, se pone la condición de salto a falso y se espera a que el jugador vuelve a estar en el suelo para cambiar de nuevo las animaciones.

No se ha limitado la intervención de fuerzas en el eje X durante el salto para que el jugador pueda maniobrar en pleno salto.

El personaje consta de animaciones para diferentes estados:

* Idle: animación por defecto en la que se espera que se produzca un Input por parte del jugador. En los assets utilizados, solo hay un sprite para dicho movimiento con lo que no se aprecia movimiento.
* Walk: animación de andar. Se han usado diferentes sprites para producir el efecto del personaje andando. Se activa  cuando el jugador da un input de movimiento en el eje horizontal.
* Jump: animación que se activa cuando el jugador quiere saltar. Se accede a su estado desde cualquiera de los otros dos y no se cambia a otro hasta que el personaje vuelve a tocar el suelo.

### 4.2. Personaje: cabeza y pies

Al personaje se le han añadido dos objetos hijo vacíos para tener mayor control con las colisiones que se producen con el entorno.

Uno de ellos corresponde a un objeto cuya "Transform" se usa para dibujar el circulo con el que discernir si el personaje se encuentra o no en el suelo. Además, dispone de un "Trigger" gracias a un "BoxCollider2D" y se usa para comprobar si los pies del jugador colisionan con un enemigo para proceder a su eliminación.

El otro se corresponde con un objeto que contiene un "BoxCollider2D" como "Trigger" y se usa para comprobar si el jugador ha colisionado con la cabeza con los diferentes tipos de bloques. Si se diera el caso, dichos bloques quedarían activados.

### 4.3. Enemigos

Los enemigos se mueven también utilizando el motor de físicas gracias a un Rigidbody2D. Además, empiezan siempre moviéndose hacia la izquierda para desplazarse primero hacia el jugador. Se ha usado un "PolygonCollider" para poder cubrir todo el sprite y tener la colisión básica con el entorno.

Poseen de un "Trigger" en la parte frontal para comprobar si, mientras se desplazan, colisionan con un objeto del entorno. En caso de que el objeto contra el que colisionan fuera parte del entorno u otro enemigo, se cambia el vector de dirección para imitar el movimiento típico de los enemigos en el juego Super Mario Bros.

En caso de que el "Trigger" entre en contacto con el jugador, se le hará daño. Eso quiere decir que, si el jugador no tiene el "SuperMode" activo, va a aparecer la UI de gameOver. De otro modo, se quitaría el "SuperMode" del jugador.

Los enemigos tienen dos animaciones, una para el movimiento horizontal y otra para cuando el jugador los elimina.

Se ha añadido una fuerza sobre el jugador en el eje vertical una vez ha sido eliminado el enemigo para simular el rebote del jugador encima del enemigo una vez le ha saltado encima. Además, la acción de eliminar a un enemigo, suma puntos al jugador.

### 4.4. Puntos de aparición de los enemigos

Para la inclusión de los enemigos, se ha desarrollado unos puntos de aparición que controlan cuántos enemigos deben aparecer en ese punto y cada cuanto tiene que aparecer un enemigo. Una vez ha hecho aparecer todos los enemigos que debía, se destruye dicho objeto.

La decisión de incluir dichos puntos ha sido para poder controlar, mediante una distancia de activación con el jugador, cuándo deben aparecer los monstruos de un lugar concreto del mapa. Gracias a éste control, no tenemos enemigos moviéndose por el mapa mientras el jugador no ha empezado la partida, puesto a que no queremos que acaben dentro de un agujero sin que el jugador los haya ni visto.

### 4.5. Bloques interactivos

Los tres bloques que se han añadido en el juego se basan en un único Prefab. Dicho objeto consta de diferentes partes:

* Un objeto padre con "SpriteRenderer", "BoxCollider2D" y "Script" de bloque.
  * El "SpriteRenderer" se actualiza en el método "Start" para poder dar la imagen del bloque en cuestión. La imagen puede variar porque se ha hecho dependiente de una variable de tipo "Enum" que define el tipo de bloque.
  * El "BoxCollider2D" para detectar las colisiones con los personajes. Los personajes deben poder andar por encima de los bloques y el jugador debe ser capaz de interaccionar con ellos si les golpea con la cabeza.
  * El "Script" con el comportamiento de los bloques. Éste script controla qué "Sprite" debe mostrar el bloque, en qué momentos debe cambiarlo y qué hacer si el jugador interacciona con él.
* Tres objetos hijo. Dos de ellos son "ParticleSystems" que dan efecto a la interacción del usuario con el bloque. El otro, se corresponde con un objeto que se utiliza para determinar la posición en la que debe aparecer el ítem que suelta el bloque.

### 4.6. SuperMode

La activación de éste modo produce dos alteraciones en el juego para que el jugador se dé cuenta de su activación. 

* La primera es una alteración en el "Pitch" de la pista de audio con la canción que se reproduce durante el juego. De ésta manera, la canción se vuelve más aguda y aumenta su ritmo. Si se desactiva el modo, el "Pitch" vuelve a su valor por defecto.
* La segunda se corresponde a la activación de un sistema de partículas del personaje. Éste sistema empieza a emitir unas partículas que usan un "Sprite" en forma de llama y envuelven al jugador mientras el modo super esté activo.

Cuando un enemigo inflige daño al jugador, se desactiva el "SuperMode" antes de acabar con el jugador. Por otro lado, si el jugador cae por un agujero, va a ser eliminado aunque tenga activo dicho modo.

Otro efecto que produce éste modo, es la aparición de un objeto "Coin" en lugar de la estrella en los bloques de exclamación. Se ha tomado ésta decisión ya que el jugador no puede acumular otro objeto de tipo "Star" y se premia de ésta manera que el jugador mantenga el estado super durante el nivel.

5. ## Cómo jugar

Al abrir el juego, vemos que se abre la escena del juego con el menú principal en pantalla. En dicho menú podemos empezar una partida o salir del juego.

Al seleccionar iniciar partida, veremos que se oculta el menú para dar lugar a una cuenta atrás que acaba por dar control del personaje al jugador.

El personaje se mueve de forma horizontal gracias a lo que Unity entiende como "Horizontal Axis", es decir, las teclas 'A' y 'D' o las flechas izquierda y derecha.

Para el salto del personaje, se puede dar una pulsación corta o larga a la tecla 'Space' del teclado. A mayor tiempo se mantenga pulsada dicha tecla más alto saltará el personaje.

Si el jugador cae por un agujero o colisiona con un enemigo, se abrirá el menú de gameOver y se le mostrarán al jugador: su puntuación actual, su puntuación récord, un botón para reiniciar el juego y otro para salir de él.

El objetivo del jugador es conseguir puntos mediante la activación de bloques moneda o eliminación de enemigos y llegar a la meta del nivel.

El jugador puede activar bloques exclamación para conseguir un objeto con el que puede recibir un toque extra por parte de los enemigos y, en caso de ya haber recogido este objeto, se sustituye por una moneda con más valor de que cualquiera de las otras fuentes de puntuación.