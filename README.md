# UOC - Juegos2D

Proyecto con los build de los diferentes proyectos entregados para la asignatura de 'Diseño de juegos 2D' del máster de la UOC 'Diseño y programación de videojuegos'. Los juegos se encuentran en el mismo estado en el cual se entregaron para la asignatura.

Para ejecutar cualquiera de los 4 juegos, sólo se debe abrir el fichero *.exe que se encuentra dentro de las carpetas Build.

Los diferentes juegos que hay son:

* PEC1 - juego de aventuras: una aventura conversacional basada en los enfrentamientos contra piratas de "Monkey Island".
* PEC2 - juego de plataformas: un juego de plataformas que recrea el nivel 1-1 del juego Super Mario Bros
* PEC3 - juego de artilleria: un juego de plataformas/disparos en el que 2 jugadores y una IA se enfrentan entre ellos para ser el último personaje en pie. Funciona por turnos, parecido al "Worms".
* PEC4 - proyecto final: un juego top-down shooter en el que el jugador debe sobrevivir a todas las oleadas de enemigos que pueda.

Dentro de todas las subcarpetas hay el fichero README original que se entregó conjuntamente con la práctica en el cuál se explica más al detalle los pasos o decisiones de desarrollo además de unas breves instrucciones que explican cómo se juega.

Espero que quién decida darles una oportunidad los encuentre entretenidos.

Saludos,

Joaquim Gifre Rosales