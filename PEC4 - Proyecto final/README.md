# PEC4 - Final Project

PEC4 correspondiente a la asignatura de Programación de Juegos 2D del máster de videojuegos de la UOC.
Para ésta práctica, se desarrollará un juego del estilo top-down shooter de supervivencia en el que el jugador deberá afrontar todas las oleadas que pueda sin morir.

---

## Índice

1. [Introducción](## Introducción)
2. [Planteamiento del juego](##Planteamiento-del-juego)
3. [Escenas del juego](##Escenas-del-juego)
4. [Detalles del desarrollo](##Detalles-del-desarrollo)
   1. [Scriptable Objects](###4.1. Scriptable Objects)
   2. [Armas: mecánicas de disparo](###4.2. Armas: mecánicas de disparo)
   3. [Armas: intercambio de armas](###4.3. Armas: intercambio de armas)
   4. [IA enemiga](###4.4. IA enemiga)
   5. [Puntos de aparición](###4.5. Puntos de aparición)
   6. [Objetos interactivos](###4.6. Objetos interactivos)
   7. [Sistema de oleadas](###4.7. Sistema de oleadas)
5. [Cómo jugar](##Cómo jugar)

---

1. ## Introducción

El proyecto desarrollado consiste en un juego del género 'shooter' con punto de vista 'top-down'. El juego consiste en una sucesión de oleadas en las que el jugador se enfrenta a los enemigos que van apareciendo y pierde en el momento en que muere.

Para conseguir sobrevivir, el jugador posee una arma con la que eliminar a dichos enemigos y con diferentes elementos del entorno que puede recoger. Éstos objetos que ayudan al jugador pueden ser diferentes armas, las cuales dotan de mayor capacidad ofensiva al jugador, u objetos de soporte (un botiquín y una caja de munición).

2. ## Planteamiento del juego

Siguiendo el enunciado proporcionado para la realización de esta [PEC](https://gitlab.com/uoc-unity-2d-2020/practica-final, se han añadido las siguientes escenas al juego:

* Una escena en la que se muestra el logo corporativo. En éste caso concreto, se ha usado el logo de la UOC con un par de transiciones y efectos sonoros.
* Una escena con el título del juego y el menú principal. En éste menú se encuentran diferentes botones para acceder al juego, ver los créditos, acceder a las opciones y salir del juego.
* La escena del nivel del juego. Consiste en un juego de supervivencia en el que el jugador se enfrenta a oleadas de zombies hasta que perece.
* Una escena con los créditos y agradecimientos.

Siendo un proyecto de temática libre, me he decidido por la realización de un 'shooter' con perspectiva top-down con las siguientes consideraciones:

* El jugador podrá desplazarse libremente en los ejes vertical y horizontal.
* La dirección que mirará el jugador dependerá de la posición del puntero del ratón en la pantalla del juego. De este modo podrá apuntar.
* El jugador podrá disparar con el arma que tenga equipada. Dicha arma tiene un cargador y una vez vaciado deberá recargarlo. Otras variables diferentes entre las armas son el daño de sus proyectiles, la cadencia de fuego del arma, la velocidad de recarga y la cantidad total de balas de dicha arma.
* El jugador va equipado por defecto con una pistola. La cantidad de munición de dicha arma es ilimitada aunque deberá recargar en cuanto dispare todas las balas del cargador. El jugador puede recargar el arma en cualquier momento, no es necesario que se quede sin balas en el cargador.
* El jugador puede equiparse con otras armas, en éste caso una escopeta o un lanzacohetes. Dichas armas tienen un tamaño de cargador diferente y munición limitada.
* En el momento en que el jugador se queda sin munición de dichas armas, se le vuelve a equipar con la pistola.
* La escopeta y el lanzacohetes tienen modo de disparo diferentes. La primera de dichas armas, dispara 3 proyectiles en un arco aleatorio (uno de los proyectiles va siempre en línea recta, los otros dos tendrán una abertura aleatoria). La segunda de dichas armas, dispara proyectiles explosivos y dañan a cualquier personaje que se encuentre dentro del radio de explosión.
* Se pueden recoger dos tipos de objeto a parte de las mencionadas armas:
  * Botiquín: recupera toda la salud del jugador.
  * Caja de munición: otorga al jugador la máxima cantidad de balas que puede tener para el arma equipada.
* Todos los objetos desperdigados por el nivel tienen un tiempo de 'cooldown'. Una vez pasado dicho tiempo, pueden volver a ser recogidos.
* El jugador y los enemigos poseen una barra de vida con la que se aprecia cuan cerca se encuentran de ser eliminados. Para el jugador se ha realizado una barra de vida fija en la pantalla mientras que para los enemigos se implementa una barra de vida más pequeña que los sigue en el juego.
* El objetivo del jugador es eliminar a todos los enemigos para poder superar la ronda y seguir todo el tiempo que pueda con vida.
* Los enemigos contarán con un mecanismo de pathfinding mediante el algoritmo A* para dirigirse al jugador y una vez lo tengan a corta distancia, poder atacarle.
* El jugador puede pausar la partida y, mediante el menú de pausa, volver al menú principal, salir del juego o seguir jugando.

3. ## Escenas del juego

En éste proyecto contamos con diferentes escenas (logo, menú principal, pantalla de juego y créditos). En ellas encontramos lo siguiente:

* Escena  logo: se muestra el logo de la UOC y un texto a través de una transición. Dichos efectos se han realizado mediante corutinas disminuyendo el alfa del color.
* Escena de título y menú principal: se muestran diferentes botones a partir de los que se puede: empezar la partida, acceder a la interficie gráfica de las opciones, ir a la escena de créditos y cerrar el juego. Al igual que para la pantalla de logo, los diferentes efectos apreciables en la GUI del menú principal se han programado mediante corutinas. Antes de acceder a la pantalla de juego, se muestra una GUI con diferentes 'tips' que cuentan cómo se juega.
* Escena del nivel de juego: mediante una sucesión de corutinas, se consigue que haya diferentes fases en el nivel: una primera fase de espera y luego la sucesión de oleadas hasta que el jugador se muere. Mientras juega, el jugador puede pausar el juego y acceder a un menú para seguir jugando, ir al menú o cerrar el juego. Una vez se muere el jugador, se oculta la GUI que informa de la vida, munición y oleada actual para mostrar el menú de GameOver. En ésta interficie de GameOver, el jugador puede ver el número de ronda a la que ha conseguido llegar, un botón para volver a empezar y un botón para regresar al menú principal.
* Escena de créditos y agradecimientos: se muestran mediante unos textos en GUI un texto de créditos y agradecimientos. Para el movimiento de la interficie, se ha creado una animación. Al final de los créditos, hay un botón con el que volver al menú principal.

4. ## Detalles del desarrollo

Para el desarrollo del juego se ha usado:

* El paquete TextMeshPro para crear textos y botones con mayor definición y más personalizables.
* El paquete "Topdown Shooter" de los assets de Kenny's del que se han sacado personajes y sprites para objetos y dibujo del mapa y  del "Top-down Tanks Redux" se han sacado los diferentes proyectiles.
* Todas las pistas de audio se han sacado de [OpenGameArt.org](https://opengameart.org/) y algunas se han editado utilizando Audacity.
* Diferentes recursos de anteriores prácticas (como las [barras de vida](https://www.youtube.com/watch?v=BLfNP4Sc_iA) o los [elementos de movimiento y personajes del juego de plataformas](https://www.youtube.com/watch?v=j111eKN8sJw)), un asset externo '[A* Pathfinding Project](https://arongranberg.com/astar/)' de Aron Granberg para el desarrollo del movimiento de la IA, el [sistema de apuntado](https://www.youtube.com/watch?v=bY4Hr2x05p8) de Blackthornprod y se ha seguido el tutorial de Brackeys para la realización del [menú de opciones](https://www.youtube.com/watch?v=YOaYQrN1oYQ) igual que el [menú de pausa](https://www.youtube.com/watch?v=JivuXdrIHK0).
* Uso de los Tilemaps y Tile Palettes para dibujar el escenario del juego de forma sencilla (se puede encontrar en el Package Manager de Unity).

### 4.1. Scriptable Objects

En éste proyecto se ha vuelto a optar por la utilización de '[ScriptableObject](https://docs.unity3d.com/Manual/class-ScriptableObject.html)' para el desarrollo de ciertos elementos. En éste caso, se ha utilizado para la implementación de las armas, proyectiles y consejos.

Para las armas, se ha implementado un ScriptableObject para almacenar las siguientes variables:

* clipSize: tamaño del cargador, es decir, el número de veces que el jugador podrá disparar el arma sin tener que recargar.
* reloadTime: tiempo que tarda en recargarse el arma. Durante este tiempo el jugador no puede disparar.
* cooldownTime: tiempo que indica la cadencia de disparo del arma del jugador, es decir, la frecuencia con la que el jugador puede disparar.
* shootSFX: clip de audio que se reproducirá cada vez que el jugador dispare el arma.
* reloadSFX: clip de audio que se reproduce cuando el jugador se encuentre recargando el arma.
* projectile: referencia a otro ScriptableObject que representa el proyectil que dispara el arma.
* weaponType: enumeración que dice el tipo de arma.

Para los proyectiles se ha implementado otro ScriptableObject el cual mantiene registro de lo siguiente:

* projectileSprite: guarda el Sprite que asociamos al proyectil.
* minDamage: guarda un valor que será el mínimo daño producido por el proyectil.
* maxDamage: guarda un valor que será el máximo daño producido por el proyectil.
* speed: guarda el valor de la velocidad constante a la que se mueve el proyectil.
* lifeTime: guarda el tiempo que puede estar el proyectil en el juego antes de ser automáticamente destruido en caso de no colisionar con nada.
* explosionRadius: guarda el valor del radio del circulo efectivo en el que el proyectil hace daño una vez activado su trigger.
* explosionPower: guarda el valor de la fuerza que ejerce el proyectil en caso de afectar a un elemento con un Rigidbody.
* isExplosion: informa de si el proyectil es de tipo explosivo para saber si reproducir los siguientes efectos de la lista.
* explosionSFX: guarda el clip de audio que se reproduce en caso de tratarse de un proyectil de tipo explosivo y ser activado por la interacción de algún 'Collider'.
* explosionVFX: guarda el 'ParticleSystem' que se reproduce en caso de tratarse de un proyectil de tipo explosivo y ser activado por la interacción de algún 'Collider'.
* isSpread: informa de si el proyectil es de tipo "Dispersión", es decir, si en caso de ser disparado, se van a disparar más copias del proyectil en ángulos aleatorios.

Para los diferentes 'tips' únicamente se ha guardado un título y un texto explicativo del consejo.

Al ser ScriptableObjects, se pueden crear diferentes objetos de éstos tipos mediante el menú de Unity. Con diferentes cambios, se pueden crear tantas  combinaciones de armas y proyectiles como se desee de forma sencilla.

### 4.2. Armas: mecánicas de disparo

En éste juego, el jugador se orienta (rota)  siguiendo la posición del puntero en la pantalla del juego. De ésta manera se gira al personaje y se puede disparar en línea recta hacia la posición deseada.

El arma tiene un punto asociado en el que se instancian los proyectiles en el momento de disparar. Dichos proyectiles tienen un 'Rigidbody2D' el cual nos ayuda a aplicarle la fuerza con la que se dispara. En éste caso, también se tienen en cuenta los diferentes modos de disparo para saber si deben instanciarse más de un proyectil o si dicho proyectil debe reproducir una explosión en el momento de colisionar.

El 'abanico' descrito en caso de ser munición del tipo 'spread', es aleatorio para dos de los tres proyectiles instanciados. Uno de ellos siempre se moverá en línea recta mientras que a los otros dos se les aplica una rotación aleatoria en el eje Z.

Además, el proyectil tiene un 'Collider2D' el cual actúa como 'Trigger'. Éste comportamiento es usado para poder explotar una vez entra en contacto con cualquier otro tipo de 'Collider'. Una vez explota el proyectil, se calcula un circulo de acción en el que se aplica daño a cualquier personaje dentro del radio de alcance. Para calcular dicho daño, se mira a qué distancia se encuentra el personaje del centro de la explosión y, a partir de dicha distancia, se le aplica un daño proporcional dentro del rango de min/max que hace el arma equipada por el jugador que ha disparado.

Además de aplicar daño a los diferentes objetos dentro del rango de acción, la explosión aplica una fuerza proporcional que va del centro de la explosión hasta el personaje en que se encuentra en la zona afectada y lo impulsa.

### 4.3. Armas: intercambio de armas

En el escenario aparecen armas sueltas que pueden ser recogidas por el personaje si les pasa por encima. Ésto le cambiaría el arma equipada y, en consecuencia, le cambiaría los parámetros de disparo.

Para calcular la interacción con otros elementos de la escena, se ha usado un 'Collider2D' a modo de 'Trigger'.

Cada arma proporciona cierto número máximo de balas al jugador correspondiente a un total de seis cargadores. Una vez ha acabado con todas las balas del arma equipada (la pistola tiene munición ilimitada) se le vuelve a equipar con la pistola.

### 4.4. IA enemiga

Se ha añadido una IA enemiga que persigue al jugador y en cuanto lo tiene a rango de ataque (el jugador entra en un 'BoxCollider2D') le ataca. 

Para el movimiento de la IA se ha utilizado la versión gratuita del asset externo 'A* Pathfinding'. Dicho paquete de Unity permite el cálculo del algoritmo A* en entornos 3D y 2D. En nuestro caso, se ha configurado para que permita el cálculo de caminos en un entorno 2D.

Los cálculos realizados por éste paquete se basan en el calculo de caminos de nodos que lleven al agente del punto A al punto B mediante el cálculo de caminos a partir de nodos.

### 4.5. Puntos de aparición

En la realización del sistema de oleadas, se han repartido por el nivel diferentes puntos por los que pueden aparecer los enemigos.

Dichos puntos son conocidos por el 'GameFlowManager' y mediante una corutina se escoge uno de forma aleatoria cada cierto tiempo y se hace aparecer un enemigo.

El número máximo de enemigos que puede haber en pantalla está limitado y no es hasta que el jugador elimina uno de los enemigos presentes en el mapa que se hace aparecer a uno nuevo aunque queden enemigos para superar la ronda.

### 4.6. Objetos interactivos

En éste juego hay diferentes tipos de juegos interactivos: armas, munición y medicinas. Todos los tipos de objeto tienen un tiempo de 'cooldown' para que, una vez recogidos, no se puedan volver a utilizar hasta pasado cierto tiempo. El tiempo de 'cooldown' de las armas es bastante superior al de los otros tipos de objeto.

Los diferentes tipos de objetos se activan mediante un 'Trigger' proporcionado por el componente de tipo 'Collider2D'. Los efectos producidos al recoger una arma son los siguientes:

* Se cambia el arma equipada por el jugador. De esta manera la munición indicada en la GUI es modificada.
* Las características de disparo se ven alteradas.
* Se dá al jugador el máximo de munición del arma recogida. Si se dispara toda la munición se vuelve a la pistola.

Los efectos producidos por la recogida de un botiquín son:

* Recuperación de los puntos de salud del jugador.

Por último, los efectos producidos por la recogida de una caja de munición son:

* Restauración al máximo de la munición del arma equipada en el momento de su recogida.

### 4.7. Sistema de oleadas

El encargado de manejar el sistema de oleadas es el ya mencionado 'GameFlowManager'. La partida se basa en los siguientes puntos:

* Unos segundos de calma antes de empezar con las oleadas.
* Una fase de oleadas ilimitadas en que solo se puede salir con la muerte del jugador (siguiendo el flujo del juego, se puede salir mediante el menú de pausa).
* Fin de la partida mediante la muerte del jugador y aparición de la GUI de GameOver en pantalla.

Todas las oleadas se basan en lo siguiente:

* Un número máximo de enemigos en pantalla de forma simultánea. No incrementa durante el transcurso del juego y se usa para evitar la acumulación excesiva de enemigos en un mapa del tamaño del juego y evitar el coste computacional excesivo del cálculo de los caminos de la IA enemiga.
* Un número de enemigos por oleada. Éste número inicia un contador que va a ir decreciendo a medida que vayan apareciendo los enemigos. Cuando se finaliza una oleada, éste número incrementa para que en la siguiente haya más enemigos a abatir durante el transcurso de la oleada.

La oleada se considera acabada cuando han aparecido ya todos los enemigos que debían aparecer y el jugador ha conseguido eliminarlos todos.

Todo el flujo del juego esta basado en diferentes corutinas y para su diseño me he basado en el sistema de flujo creado en el proyecto de Unity llamado Tanks.

5. ## Cómo jugar

Al abrir el juego, vemos que se nos aparece la escena de presentación con el logo y el nombre del desarrollador antes de ir a la escena con el menú principal.

Una vez en el menú principal, vemos que se reproducen una serie de efectos que acaban mostrando las diferentes opciones de un menú principal. Entre dichas opciones vemos:

* Start Game: botón que nos muestra unos consejos y luego nos lleva a la escena del juego.
* Options: botón que nos muestra la GUI con los diferentes ajustes que podemos cambiar. Se incluyen el volumen genérico, la resolución y calidad y el modo 'windowed'.
* Credits: botón que nos lleva a la escena con los creditos. En ella podemos ver un botón para volver al menú principal.
* Exit: botón para salir del juego.

Al seleccionar iniciar partida, vemos primero unos consejos que nos indican cómo se juega y en qué consiste el juego.

El personaje del jugador, se mueve de forma horizontal y vertical gracias a lo que Unity entiende como "Horizontal/Vertical Axis", es decir, las teclas 'A', 'S', 'W' y 'D' o las flechas izquierda, abajo, arriba y derecha.

Para poder disparar, debe apuntarse con el ratón y apretar el botón izquierdo del ratón. Se puede recargar el arma en cualquier momento utilizando la tecla 'R'.

El objetivo del juego es sobrevivir el máximo número de rondas posible utilizando los recursos que se encuentran en el mapa.

Se puede pausar el juego con la tecla 'Esc'. En el menú de pausa se puede reanudar el juego, ir al menú o salir del juego.