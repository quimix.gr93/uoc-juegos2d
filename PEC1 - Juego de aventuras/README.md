* # PEC1 - Adventure Game

  Proyecto que corresponde al desarrollo de la PEC1 - Un juego de aventuras, de la asignatura de Programación en Unity 2D del máster de diseño de videojuegos de la UOC

  ---
## Índice

1. [Introducción](## Introducción)
2. [Planteamiento del juego](##Planteamiento-del-juego)
3. [Escenas del juego (Scenes)](##Escenas-del-juego-(Scenes))
4. [Detalles del desarrollo](##Detalles del desarrollo)
   1. [Insultos](###4.1. Insultos)
   2. [Máquina de estados finitos (FSM)](###4.2. Máquina de estados finitos (FSM))
   3. [GameFlowController](###4.3. GameFlowController)
   4. [GameSessionController](###4.4. GameSessionController)
   5. [Patrón Singleton](###4.5. Patrón Singleton)
   6. [Estado actual y previo](###4.6. Estado actual y previo)
5. [Cómo jugar](##Cómo jugar)

---

1. ## Introducción

  El proyecto que se ha desarrollado consiste en una aventura conversacional en la que el jugador debe ganar a un rival en una pelea de insultos. Dicha pelea consistirá en un "duelo" por turnos, intercalando jugador y rival, en el que ganará el mejor de 3, es decir, se sucederán hasta que uno de los dos participantes del duelo consiga los 3 puntos.

  Los insultos que se usarán son los pertenecientes al juego Monkey Island.

  2. ## Planteamiento del juego

  Partiendo de la información proporcionada en el enunciado de la PEC, las decisiones que he tomado a nivel del juego son las siguientes:

  * Consistirá en un juego por turnos en el que un jugador propone una opción de la lista de insultos y su oponente debe proporcionar una de las tres respuestas a la opción dada.
  * Acertar la respuesta da un punto y ser el primero en actuar en la siguiente ronda.
  * El hecho de fallar en dar respuesta, suma punto al oponente y él empezará la roda siguiente.
  * Se facilita la lista completa de insultos a el jugador que empieza el turno. Cada vez que se selecciona un insulto de la lista (ya sea el jugador o el oponente) se elimina dicho insulto de la lista para evitar repeticiones.
  * Las puntuaciones tanto de jugador como de oponente se muestran en pantalla en formato de barras de vida, puesto que es un juego de pelea, y el primero en quedarse sin 'vida' pierde.

  3. ## Escenas del juego (Scenes)

  Para el juego en cuestión se han realizado tres escenas:

  * Main Menu: escena principal en la que el jugador puede empezar juego o salir de él a partir de dos botones.
  * Game: escena en la que transcurre el juego. Aquí se presenta al usuario una interfaz completa en la que puede:
    * observar las barras de 'vida' tanto suya como del oponente, que son representativas del sistema de puntuación.
    * observar un título en la interfaz que indica si es el turno del jugador para empezar a insultar o si debe darle respuesta.
    * ver la lista de posibilidades para empezar una ronda o como para contestar al oponente. Se mostrará una u otra dependiendo de en que estado de la ronda nos encontremos.
  * Game Over: escena en la que, además de ver quién ha ganado la partida, tiene dos botones, uno para volver a empezar y otro para volver al menú principal.

  4. ## Detalles del desarrollo

  Para el desarrollo del juego se han usado:

  * El paquete TextMeshPro para poder crear textos y botones mejor definidos y con mayor posibilidad de personalización.
  * Un asset gratuito de la AssetStore para poder añadir imágenes en los campos de la UI y que ésta quede más bonita.
  * Se ha partido del tutorial de Brackeys en youtube para el desarrollo de las [barras de vida](https://www.youtube.com/watch?v=BLfNP4Sc_iA&t=793s) (sistema de puntuaje del juego).
  * Se ha diseñado una máquina de estados finitos para el transcurso de los turnos de la partida.
* Todos los insultos se guardan en un fichero JSON.
  

  
### 4.1. Insultos

El juego gira en torno de los insultos, dichos insultos se encuentran almacenados en un fichero *.json en la carpeta Resources del proyecto.

El fichero se carga utilizando la libreria JsonUtility de Unity y se guarda en un objeto de tipo Swears que contiene un array de objetos tipo Swear. Un insulto (Swear) consta de los siguientes atributos:

  * openingLine: string que representa la entrada que escoge el jugador/rival.
  * answers: un array de tipo string en el que se almacenan las posibles respuestas al insulto.
* correctIndex: un entero que guarda registro de qué respuesta es la correcta.
  

  
### 4.2. Máquina de estados finitos (FSM)

Para implementar la máquina de estados, se ha creado una interfície estado con diferentes métodos. Los métodos de los que consta un estado son los siguientes:

  * UpdateState(): este estado es el encargado de evaluar los resultados de la acción del jugador. Ya sea llamado en el método Update() del GameFlowController si es un turno del rival o después de que el jugador haga click en una opción. Una vez evalua la acción del jugador/rival lo dirige al próximo estado.
  * ToPlayerTurnState(): aquí cambiamos el estado actual a estado PlayerTurnState.
  * ToPlayerAnswerTurnState(): se cambia el estado actual a PlayerAnswerTurnState.
  * ToRivalTurnState(): se mueve la máquina de estados al etado RivalTurnState.
* ToRivalAnswerTurnState(): se cambia el estado a RivalAnswerTurnState.
  

Las transiciones posibles entre estados de esta FSM quedan reflejados en el siguiente gráfico:

  ```mermaid
  stateDiagram
  	StartState
  	PlayerTurnState
  	PlayerAnswerTurnState
  	RivalTurnState
  	RivalAnswerTurnState
  	
  	StartState --> PlayerTurnState
  	StartState --> RivalTurnState
  	PlayerTurnState --> RivalAnswerTurnState
  	RivalTurnState --> PlayerAnswerTurnState
  	PlayerAnswerTurnState --> PlayerTurnState
  	PlayerAnswerTurnState --> RivalTurnState
  	RivalAnswerTurnState --> PlayerTurnState
  	RivalAnswerTurnState --> RivalTurnState
  ```

  * StartState: en este estado, la máquina decide de forma aleatoria qué jugador empieza el 'duelo' con un Random.Range() y dirige o a PlayerTurnState o RivalTurnState.
  * PlayerTurnState: en este estado, el juego espera a que el jugador seleccione uno de los insultos de la lista para luego pasar al turno de RivalAnswerTurnState.
  * RivalTurnState: en este estado, el rival selecciona de forma aleatoria uno de los insultos de la lista para pasar luego al turno de PlayerAnswerTurnState.
  * PlayerAnswerTurnState: en este estado, el jugador debe seleccionar una de las tres opciones posibles para contestar al insulto del rival. Si éste acierta, se mueve a PlayerTurnState y se le quita vida al rival. Si por contrario falla en su elección, se quita vida al usuario y se mueve al estado RivalTurnState.
* RivalAnswerTurnState: en este estado, el rival selecciona de forma aleatoria una de las tres opciones posibles para contestar al insulto del jugador. Para hacer esto se utilizan 2 randoms, un primer random para decidir si automáticamente el rival acierta en responder al jugador y otro para que, en caso de no acertar directamente, saque un índice de los tres posibles para responder de forma aleatoria. De este modo, se complica un poco más el poder ganar que si solamente se saca un índice aleatorio. Una vez tiene la respuesta, se calcula el daño al jugador correspondiente y se mueve o a RivalTurnState si ha acertado o PlayerTurnState si ha fallado.
  

Se entiende como ronda de la partida al conjunto de secuencias que pueden ser:

  ```shell
  PlayerTurnState -> RivalAnswerTurnState
  ó
RivalTurnState -> PlayerAnswerTurnState
  ```

  

### 4.3. GameFlowController

Script principal del transcurso del juego. En este script encontramos los principales métodos Awake, Start y Update del flujo del juego.

* Awake(): iniciamos los diferentes estados que va a tener nuestra FSM.
* Start(): reseteamos la sesión de juego, seleccionamos la UI que se va a mostrar al usuario y se rellenan las opciones disponibles (hay dos componentes principales en la UI, los que muestran las opciones del estado PlayerTurnState y los que muestran las respuestas para PlayerAnswerTurnState), empezamos la reproducción de la música del juego y inicializamos las barras de vida(UI).
* Update(): se comprueba si hay ganador para llamar a la corutina de llevar al usuario a la pantalla de GameOver y se comprueba que el estado sea StartGameState, RivalTurnState o RivalAnswerTurnState para que se llame automáticamente al UpdateState() de ese estado.

Se hace uso de una corutina para mandar al usuario a la pantalla de fin de juego para poder utilizar:

```c#
yield return new WaitForSeconds(int amount)
```

De esta manera, el usuario vé que se le ha agotado la barra de vida al rival antes de ser llevado a la escena de fin de juego. De otro modo, en conseguir agotar la vitalidad del rival (o del jugador), el jugador sería llevado automáticamente a la pantalla de GameOver sin noción de lo sucedido.

Los otros métodos incluidos en el script están relacionados con la actualización de la interfaz de usuario, ya sean los textos, opciones o barras de vida, actualizar el estado previo, reproducir SFX y el listener de los botones de las opciones.

### 4.4. GameSessionController

Este script se encarga de leer el fichero con los insultos y trata de almacenar toda la información relativa al estado del juego. Dichos datos incluyen:

* swears: lista de insultos actuales en el juego. Esta lista se vé mermada cada vez que el jugador o rival usan un insulto en el PlayerTurnState o RivalTurnState.
* currentSwear: mantiene registro del insulto que está actualmente activo en una ronda de juego.
* maxHealth: variable que define el máximo de salud que tendrán ambos jugadores.
* playerCurrentHealth: variable que define la salud actual del jugador.
* rivalCurrentHealth: variable que define la salud actual del rival.

En este script encontramos los métodos necesarios para cargar los insultos al juego, obtener los valores de salud tanto máximo como de los jugadores, hacer daño a un jugador, comprobar si hay ganador y decir quién es.

### 4.5. Patrón Singleton

Se ha usado el patrón Singleton tanto con el objeto que contiene el GameSessionController como el controlador del audio del juego.

Dicho patrón permite que haya una única instancia activa de un objeto en el transcurso del juego, aún habiendo transacciones entre escenas. De esta manera, la puntuación del juego es visualizada en la escena GameOver sin problema aún proviniendo de la escena Game.

El patrón Singleton se basa en lo siguiente:

```c#
private static GameSessionController _instance;
public static GameSessionController Instance
{
    get { return _instance; }
}

private void Awake()
{
    SetUpSingleton();
}

private void SetUpSingleton()
{
    if (_instance != null && _instance != this)
    {
        Destroy(gameObject);
        return;
    }

    _instance = this;
    DontDestroyOnLoad(gameObject);
}
```

Es decir, si ya existe una instancia del objeto se le indica a Unity que lo destruya, sinó se marca dicho objeto para que no sea destruido al realizar una carga.

### 4.6. Estado actual y previo

El juego almacena referencia tanto del estado actual de la ronda como de su estado previo. La inclusión del estado previo se debe a que, cuándo se trata del turno del rival, para mantener unos textos situacionales diferentes que dependieran de la situación, debía conocer no sólo del estado actual de la ronda sinó de su estado anterior.

5. ## Cómo jugar

Al abrir el juego, se abre el menú principal. En él podemos, o bien, salir del juego, o bien, empezar la partida.

En el momento que se empieza la partida, el usuario visualiza un rectángulo en el que puede distinguir las siguientes partes:

-  Título: hay un título en la cima del rectángulo en el que puede leer "Dale donde duele!" o "Hora de responder!" dependiendo de si es turno del jugador para empezar la ronda o de contestar al rival.
- Texto situacional: texto que se actualiza cada vez que cambia de estado del juego, de esta manera, puede leer la introducción, el insulto del rival o sus reacciones.
- Botones de insulto: dichos botones se encuentran debajo del texto situacional y pueden, o bien ser insultos para empezar la ronda (aparecen con scroll vertical), o bien, las respuestas al insulto del rival (aparecen sólo 3).

Sabiendo en que parte de la ronda se encuentra el jugador, éste debe seleccionar una de las opciones teniendo en cuenta otro elemento importante de la UI, las barras de vida.

Las barras de vida indican cuán cerca está uno de los participantes en el duelo de la victoria o derrota. Igual que en otros juegos de pelea:

* Si el jugador se queda sin vida, pierde.
* Si el rival se queda sin vida, el jugador gana.

Sea cual sea el resultado de la pelea, el jugador es llevado a la pantalla de GameOver. En ésta pantalla, se le muestra un mensaje indicándole quién es el ganador de la riña y se le ofrecen dos opciones en forma de botón, empezar de nuevo el juego o volver al menú principal.