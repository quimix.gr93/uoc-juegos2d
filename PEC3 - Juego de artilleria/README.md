# PEC3 - Artillery Game

Proyecto que corresponde al desarrollo de la 'PEC3 - Un juego de artillería' para la asignatura Programación de juegos Unity 2D de la UOC del semestre febrero 2020

---

## Índice

1. [Introducción](## Introducción)
2. [Planteamiento del juego](##Planteamiento-del-juego)
3. [Escenas del juego](##Escenas-del-juego)
4. [Detalles del desarrollo](##Detalles-del-desarrollo)
   1. [Armas: Scriptable Object](###4.1. Armas: Scriptable Object)
   2. [Armas: mecánicas de disparo](###4.2. Armas: mecánicas de disparo)
   3. [Armas: intercambio de armas](###4.3. Armas: intercambio de armas)
   4. [IA enemiga](###4.4. IA enemiga)
   5. [Puntos de aparición](###4.5. Puntos de aparición)
   6. [Bloques interactivos](###4.6. Bloques interactivos)
   7. [Sistema de rondas](###4.7. Sistema de rondas)
5. [Cómo jugar](##Cómo jugar)

---

1. ## Introducción

El proyecto que se ha desarrollado consiste en un juego del género 'shooter' parecido al clásico 'Worms'. El juego consiste en una sucesión de rondas en la que el primero de los participantes que llegue a ser el único personaje en pie gana un punto. Si uno de los personajes acumula 5 puntos, éste se declara como ganador y se acaba el juego.

Para conseguir dicho fin, los personajes en pantalla se disparan los unos a los otros con una de las diferentes armas que se han implementado, en éste caso un lanzacohetes y una escopeta. Cada una de dichas armas, cuenta con diferentes propiedades como son el daño que hacen, el tipo de disparo que realizan (cargado o no) y número de disparos por turno.

2. ## Planteamiento del juego

A partir de la información que se proporciona en el enunciado de la [PEC](https://gitlab.com/uoc-unity-2d-2020/pec-3-un-juego-de-artilleria), se han tomado las siguientes decisiones al respecto del juego:

* Se usarán las mecánicas de movimiento de la PEC2 para aprovechar las físicas de Unity como forma de desplazarse horizontalmente y saltar. Se ha mantenido la peculiaridad del salto para saltar más a mayor tiempo de pulsación de la tecla de salto.
* El juego consistirá en una arena cerrada con diferentes plataformas. Se quiere que la cámara mantenga enfocados siempre a todos los participantes activos de la ronda. Para desarrollar dicha característica, se ha implementado la cámara del proyecto Tanks de Unity.
* Se ha implementado una IA capaz de perseguir a los demás jugadores y dispararles si se encuentran a la distancia de disparo y hay línea clara de disparo.
* Se han añadido dos elementos destructibles en el escenario:
  * explosivos: si se encuentran dentro del radio de explosión de uno de los proyectiles disparados por los participantes, éste también explotará y hará daño a los personajes que se encuentren dentro de su área de acción.
  * de piedra: obstáculos sencillos cuya finalidad es entorpecer la línea de visión de los jugadores para ofrecer cobertura temporal. Si se encuentran dentro del radio de alguna explosión, serán destruidos.
* Cada participante tiene una barra de vida que refleja los puntos de daño que pueden recibir durante cada ronda. Siempre se mantiene en pantalla para ver el estado de cada participante. Siempre se mantiene mirando a cámara, de modo que no gira si el personaje va en un sentido u otro. Se ha usado la [barra de vida de Brackeys](https://www.youtube.com/watch?v=BLfNP4Sc_iA) ya usada en la PEC1 de la asignatura.
* La IA enemiga se moverá mediante el uso del asset externo ya mencionado anteriormente. Dicho asset se ha conocido a partir de otro vídeo de [Brackeys](https://www.youtube.com/watch?v=4T7KHysRw84).
* Se han incluido diferentes armas y los personajes en escena pueden cambiar de arma si pasan por encima de dichas armas en el escenario. Hacer ésto les cambia la arma equipada y con ello, las características del disparo se ven alteradas.
* Se añade un indicador con forma de flecha para indicar qué jugador tiene el turno activo.
* Aprovechando la mecánica de Tanks, se ha hecho un juego en el que hay dos jugadores jugables y un tercero que se corresponde a la IA enemiga.

Se han creado dos tipos de armas y son las siguientes:

* Escopeta: sus proyectiles hacen poco daño pero se permite disparar 2 veces. El disparo no se puede cargar y hay tiempo de 'cooldown' entre disparos.
* Lanzacohetes: sus proyectiles hacen mucho daño y solamente se puede disparar una vez. El disparo a realizar es de tipo cargado y a mayor tiempo de pulsación, con más impulso sale disparado el proyectil.

El juego consiste en una sucesión de rondas. Dichas rondas se dividen en turnos en los que cada personaje va a poder actuar durante cierto tiempo o hasta que haya disparado todos los proyectiles que le permite su arma. Los turnos de cada ronda se van a ir sucediendo hasta que sólo uno de los personajes siga en pie (o ninguno).

En caso de que quede un jugador en pie, se le sumará un punto y en caso de acumular 5 se le considerará ganador del juego. En caso de que nadie quede en pie, no se suma punto a nadie.

3. ## Escenas del juego

Se ha seguido el ejemplo de la PEC2 y se ha desarrollado un juego con una única escena. Esta escena cuenta con todas la UI que intervienen en el juego. Entre estas UI encontramos:

* Menú inicio: se puede apreciar el título del juego en una pequeña interfaz y dos botones. Uno para empezar a jugar y otro para salir del juego.
* Mensajes de ronda: UI consistente de un texto en el que se muestran los mensajes para empezar la ronda, las puntuaciones de los diferentes participantes una vez ha acabado la ronda y el mensaje al final de la partida con el nombre del ganador. 
* Menú de fin de juego: se muestra conjuntamente con el mensaje de ganador mencionado anteriormente y le añade dos botones. Uno de los botones devuelve el juego a su estado inicial y el otro permite salir del juego.
* Temporizador: aparece en la esquina superior derecha y muestra los segundos que quedan para finalizar el turno. El temporizador se reinicia a cada turno.

4. ## Detalles del desarrollo

Para el desarrollo del juego se ha usado:

* El paquete TextMeshPro para crear textos y botones con mayor definición y más personalizables.
* El paquete "Platformer Pack Redux" de assets de Kenny's del que se han sacado personajes y sprites para objetos y dibujo del mapa.
* Diferentes recursos de anteriores prácticas (como las [barras de vida](https://www.youtube.com/watch?v=BLfNP4Sc_iA) o los [elementos de movimiento y personajes del juego de plataformas](https://www.youtube.com/watch?v=j111eKN8sJw)), elementos de otros proyectos de Unity (en éste caso concreto se ha aprovechado el proyecto [Tanks](https://learn.unity.com/project/tanks-tutorial) y se ha copiado su mecánica de rondas y su cámara), un asset externo '[A* Pathfinding Project](https://arongranberg.com/astar/)' de Aron Granberg para el desarrollo del movimiento del jugador IA y el [sistema de apuntado](https://www.youtube.com/watch?v=bY4Hr2x05p8) de Blackthornprod.
* Uso de los Tilemaps y Tile Palettes para dibujar el escenario del juego de forma sencilla (se puede encontrar en el Package Manager de Unity).

### 4.1. Armas: Scriptable Object

Para la implementación de las diferentes armas, se ha trabajado con '[ScriptableObject](https://docs.unity3d.com/Manual/class-ScriptableObject.html)'. Usar 'ScriptableObjects' permite poder crear diferentes armas con los atributos necesarios para que cada una de ellas sean diferentes y asignarlas al script que controla la mecánica de disparo para que los atributos de cada tipo de arma resulte diferentes.

Los parámetros que se han considerado para éste tipo de desarrollo han sido los siguientes:

* name: campo que tiene el nombre de la arma en cuestión.
* weaponSprite: variable de tipo 'Sprite' con la que se permite mostrar el arma que tiene equipada el personaje.
* minDamage/maxDamage: variables de tipo int que se mantienen dentro de unos rangos y sirven para calcular el daño que puede realizar el disparo producido por el arma.
* projectileLifeTime: variable de tipo float que sirve para calcular el tiempo máximo que el proyectil  tiene antes de destruirse.
* timeBetweenShots: variable de tipo float que indica el tiempo de 'cooldown' del arma antes de poder ser disparada de nuevo.
* chargeShoot: variable de tipo boleano que indica si el tipo de disparo a realizar es de tipo cargado o no.
* maxChargeTime: variable que indica el tiempo máximo que se puede cargar el disparo del proyectil.
* timesToShoot: variable de tipo entero que indica el número de veces que se puede disparar en un turno con el arma equipada.

Al ser un ScriptableObject, se pueden crear diferentes objetos de éste tipo mediante el menú de Unity. Con diferentes cambios, se pueden crear tantas armas como se desee de forma sencilla. En éste caso concreto, se ha creado la escopeta y el lanzacohetes.

### 4.2. Armas: mecánicas de disparo

Como se ha mencionado anteriormente, se han implementado dos mecánicas diferentes para el disparo:

* Disparo cargado: consiste en mantener pulsado el botón de disparo durante *X* tiempo para alterar la fuerza con la que se va a disparar el proyectil. Hay un tiempo máximo para cargar el disparo y en caso de no soltar el botón de disparo, se dispara automáticamente pasado dicho tiempo. Se muestra una flecha que aumenta de tamaño según el tiempo de carga y apunta en dirección al lugar deseado.
* Disparo normal/no cargado: consiste en pulsar el botón de disparo para soltar un proyectil. Al no poder cargar el disparo, se aplica el máximo de impulso declarado en el arma. En caso de tener la posibilidad de realizar más de un disparo por turno, se tiene en cuenta el tiempo de 'cooldown' entre disparos antes de dejar disparar otra vez al jugador.

El modo de apuntado se basa en el cálculo de un vector dirección a partir de la posición del personaje activo y la ubicación del ratón en pantalla. Una vez calculada dicha dirección, se rota el arma para que mire en esa dirección.

El arma tiene un punto asociado en el que se instancian los proyectiles en el momento de disparar. Dichos proyectiles tienen un 'Rigidbody2D' el cual nos ayuda a aplicarle la fuerza con la que se dispara y permite al sistema de gravedad de Unity afectar a dicho objeto.

Además, el proyectil tiene un 'Collider2D' el cual actúa como 'Trigger'. Éste comportamiento es usado para poder explotar una vez entra en contacto con cualquier otro tipo de 'Collider'. Una vez explota el proyectil, se calcula un circulo de acción en el que se aplica daño a cualquier personaje dentro del radio de alcance. Para calcular dicho daño, se mira a qué distancia se encuentra el personaje del centro de la explosión y, a partir de dicha distancia, se le aplica un daño proporcional dentro del rango de min/max que hace el arma equipada por el jugador que ha disparado.

Además de aplicar daño a los diferentes objetos dentro del rango de acción, la explosión aplica una fuerza proporcional que va del centro de la explosión hasta el personaje en que se encuentra en la zona afectada y lo impulsa. A parte de otros jugadores, las explosiones afectan a los diferentes bloques que hay en escena.

Se ha desactivado de la matriz de físicas la interacción del proyectil con las barreras delimitadoras del mundo. De esta manera no se pueden aprovechar las colisiones con el muro para influir en la colisión del proyectil y hacer daño a otros personajes.

### 4.3. Armas: intercambio de armas

En el escenario aparecen armas sueltas que pueden ser recogidas por un personaje si les pasa por encima. Ésto les cambiaría el arma equipada y, en consecuencia, le cambiaria los parámetros de disparo.

Para calcular la interacción con otros elementos de la escena, se ha usado un 'Collider2D' a modo de 'Trigger'. En caso de ser activado por un jugador activo, se le equipa el arma con la que ha colisionado y ésta hasta que finalice la ronda.

Una vez una arma es recogida, se desactiva el objeto y se vuelven a reactivar cuando comience la siguiente ronda.

### 4.4. IA enemiga

Se ha añadido una IA enemiga que persigue a los rivales y les dispara si tiene una línea de visión clara y se encuentran dentro del rango de disparo. Al igual que los personajes jugables, tiene un 'Rigidbody2D' con el que se le aplican fuerzas para realizar los movimientos horizontales y verticales y que permite la aplicación de gravedad por parte de Unity sobre el agente.

Para el movimiento de la IA se ha utilizado la versión gratuita del asset externo 'A* Pathfinding'. Dicho paquete de Unity permite el cálculo del algoritmo A* en entornos 3D y 2D. En nuestro caso, se ha configurado para que permita el cálculo de caminos en un entorno 2D.

Los cálculos realizados por éste paquete se basan en el calculo de caminos de nodos que lleven al agente del punto A al punto B mediante el cálculo de caminos a partir de nodos. Aún así, es un paquete que no está pensado para ser utilizado en juegos de tipo plataforma, con lo que se han tenido que añadir diferentes comprobaciones para hacer que se consiga más o menos el tipo de movimiento deseado.

Las comprobaciones añadidas han sido las siguientes:

* No se aplica fuerza para mover al personaje si la dirección de dicha fuerza es negativa en el eje *Y* y además el agente no se encuentra con los pies en el suelo (isGrounded). Se considera en éste caso que el agente está cayendo.
* Si el vector de dirección supera el 0.5 en el eje *Y* se considera que se intenta realizar un salto y la fuerza aplicada sobre el agente es mucho mayor.
* Para cualquier otro tipo de dirección se aplica una fuerza más reducida que asemeja el movimiento horizontal del agente al realizado por un personaje del jugador.

El personaje consta de animaciones para diferentes estados ayudado de las consideraciones anteriores:

* Walk: animación de andar. Es la misma que usan los personajes jugables. Se activa cuando la dirección que decide seguir para ir del nodo actual al siguiente no cumple ninguna de las 2 primeras condiciones mencionadas anteriormente.
* Jump: animación que se activa cuando el personaje quiere emular el salto o está en caída. Se activa esta animación si se cumple alguna de las dos primeras condiciones mencionadas.

Para la realización del disparo, el personaje controlado por la IA mantiene una lista con todos los objetivos a abatir. En ésta lista se encuentran únicamente personajes controlados por el jugador con lo que de querer añadir más agentes, se debería cambiar parte del código.

De entre los diferentes objetivos, el personaje controlado por la IA selecciona uno de la lista. Éste objetivo será el destino con el que se calcula el camino para el movimiento del agente. Para saber si debe disparar, comprueba que dicho objetivo se encuentre dentro de la distancia de disparo y además, traza una línea que va del agente al objetivo. Si la primera intersección que hace la línea es con el objetivo, le dispara, de otra manera, persigue al objetivo usando el camino a seguir proporcionado por la librería externa.

En caso de encontrarse con un obstáculo, el agente está programado para dispararlo automáticamente.

### 4.5. Puntos de aparición

Al estar usando un sistema de juego parecido al mostrado en el proyecto Tanks de Unity, cada uno de los participantes en el juego tiene un punto de aparición particular.

Al empezar el juego, los diferentes participantes de la partida son instanciados en su correspondiente punto de aparición y son devueltos a esa ubicación al acabar la ronda.

### 4.6. Bloques interactivos

Se han implementado dos bloques diferentes para su interacción con los proyectiles disparados por los diferentes personajes.

* Bloques de tierra: son un mero obstáculo a los disparos efectuados por los diferentes jugadores. Se destruyen si están dentro del radio de acción de alguna explosión y usan un sistema de partículas para dar mayor efecto a su destrucción.
* Bloques explosivos: éstos bloques producen una explosión a parte de la producida por el proyectil. Por consiguiente, aplican daño a los diferentes personajes que se encuentren dentro de su radio de acción y les aplica la fuerza de empuje del mismo modo que haría la explosión producida por un proyectil.

Ambos bloques no reaparecen una vez finaliza la ronda. Debe reiniciarse el juego para que vuelvan a aparecer. Ésto se ha hecho para no permitir al jugador abusar del disparo de lanzacohetes sumado a la explosión del bloque para poder eliminar a otro participante de un único disparo.

### 4.7. Sistema de rondas

Partiendo del sistema de rondas del proyecto Tanks de Unity, se ha implementado un sistema de rondas a partir del que los personajes van sucediéndose en una serie de turnos hasta que solamente uno de los personajes sigue en pie. Dicho personaje gana la ronda y en caso de haber ganado 5 rondas gana la partida.

Para que se sucedan los turnos, se usan dos listas diferentes: una primera lista para los diferentes participantes que van a poder ser controlados por el jugador y una segunda lista de elementos controlados por IA.

Los turnos de los participantes se basan en una ordenación aleatoria de dichas listas. Aún así, primero juegan siempre los personajes controlados por el jugador y luego las IA. Eso se debe a que se usan listas de elementos homogéneas para los jugadores y las IA.

Para saber qué tipo de turno se está jugando, hay una variable de tipo enum llamada PlayerType que registra si quien se mueve en el turno actual es un jugador o IA. De esta manera, al cambiar de turno se sabe de que tipo de personajes se debe incrementar el índice para seleccionar el próximo en jugar.

Una vez se llega al final de la lista de un tipo de personajes, la enumeración cambia de tipo y se empieza a seleccionar elementos de la otra lista. Además se usa una condición que comprueba si el próximo jugador a mover sigue vivo. De ésta manera, si el jugador ha muerto se puede buscar otro jugador recorriendo las listas pertinentes que sí esté activo.

El turno de un personaje puede acabar por dos motivos diferentes:

* Ha disparado todas las veces que su arma le permite.
* Se le ha acabado el tiempo para realizar sus acciones.

En ambo casos, se busca al siguiente jugador como se ha mencionado.

Los turnos se suceden hasta que sólo uno de los participantes queda en pie. Dicho personaje sumará un punto a sus victorias. En caso de haber empate, ninguno de los jugadores ganará punto. El sistema de rondas hace que se sucedan de forma ilimitada hasta que uno de los participantes llegue a las 5 victorias

5. ## Cómo jugar

Al abrir el juego, vemos que se abre la escena del juego con el menú principal en pantalla. En dicho menú podemos empezar una partida o salir del juego.

Al seleccionar iniciar partida, veremos que se oculta el menú para mostrar el primer mensaje que nos indica que una ronda está a punto de empezar y el número de la ronda que está por comenzar.

Los personajes controlados por el jugador, se mueven de forma horizontal gracias a lo que Unity entiende como "Horizontal Axis", es decir, las teclas 'A' y 'D' o las flechas izquierda y derecha.

Para el salto del personaje, se puede dar una pulsación corta o larga a la tecla 'Space' del teclado. A mayor tiempo se mantenga pulsada dicha tecla más alto saltará el personaje.

Para poder disparar, debe apuntarse con el ratón y apretar el botón izquierdo del ratón.

Si el jugador tiene el lanzacohetes, puede dejar pulsado el botón del ratón para cargar el disparo. De esta manera, el impulso del proyectil será mayor. Si lo mantiene más tiempo del que puede cargar, se disparará automáticamente.

Si el jugador tiene la escopeta, la simple pulsación del botón izquierdo del ratón hace que el personaje dispare. Se puede disparar dos veces respetando el tiempo de 'cooldown'.

El objetivo del juego es conseguir 5 victorias en las diferentes rondas que se van a producir.